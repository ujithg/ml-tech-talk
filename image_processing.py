import cv2
from darkflow.net.build import TFNet
import matplotlib.pyplot as plt
import numpy as np
import time

options = {
    'model': 'cfg/yolo.cfg',
    'load': 'bin/yolov2.weights',
    'threshold': 0.3,
    'gpu': 1.0,
}

tfnet = TFNet(options)

img = cv2.imread('sample_img/sample_person.jpg', cv2.IMREAD_COLOR)
result = tfnet.return_predict(img)
img.shape

colors = tuple(255 * np.random.rand(3) for i in range(100))

for r, c in zip(result, colors):
    tl = (r['topleft']['x'], r['topleft']['y'])
    br = (r['bottomright']['x'], r['bottomright']['y'])
    label = r['label']

    img = cv2.rectangle(img, tl, br, tuple(c), 2)
    img = cv2.putText(img, label, tl, cv2. FONT_HERSHEY_SIMPLEX, 1, (255, 255, 225), 2)

img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
plt.imshow(img)
plt.savefig('sample_img/out/sample_person.jpg', dpi=480)
